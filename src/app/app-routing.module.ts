import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'demo-lifecycle', loadChildren: () => import('./features/demo-lifecycle/demo-lifecycle.module').then(m => m.DemoLifecycleModule) },
  { path: 'demo-uikit1', loadChildren: () => import('./features/demo-uikit1/demo-uikit1.module').then(m => m.DemoUikit1Module) },
  { path: 'demo-uikit2', loadChildren: () => import('./features/demo-uikit2/demo-uikit2.module').then(m => m.DemoUikit2Module) },
  { path: '', redirectTo: 'demo-lifecycle', pathMatch: 'full'},
  { path: 'catalog', loadChildren: () => import('./features/catalog/catalog.module').then(m => m.CatalogModule) }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
