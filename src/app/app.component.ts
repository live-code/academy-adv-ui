import { Component, ElementRef, TemplateRef, ViewChild, ViewContainerRef } from '@angular/core';
import { ThemeService } from './core/services/theme.service';
import { AuthService } from './core/services/auth.service';

@Component({
  selector: 'ac-root',
  template: `
    <button routerLink="demo-lifecycle" routerLinkActive="bg-warning">lifecycle</button>
    <button routerLink="demo-uikit1" routerLinkActive="bg-warning">uikit1</button>
    <button routerLink="demo-uikit2" routerLinkActive="bg-warning">uikit2</button>
    
    <button
      routerLink="catalog" 
      acMyRouterLinkActive="bg-warning">catalog</button>
    <button (click)="themeService.value = 'dark'">D</button>
    <button (click)="themeService.value = 'light'">L</button>
    <button (click)="authService.login()">Login </button>
    <button *acIfLogged="'admin'" (click)="authService.logout()">Quit</button>
    
    
    <hr>
    <div class="container">
      <router-outlet></router-outlet>
    </div>
  `,
  styles: []
})
export class AppComponent {
  constructor(
    public themeService: ThemeService,
    public authService: AuthService,
  ) {

  }


}
