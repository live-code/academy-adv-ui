import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Product } from '../../../../model/product';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'ac-catalog-form',
  template: `
    <form #f="ngForm" (submit)="saveHandler(f)">
      <input type="text" [ngModel]="active?.name" name="name">
      <button type="submit">SAVE</button>
      <button type="button" (click)="resetHandler(f)">reset</button>
    </form>
  `,
})
export class CatalogFormComponent {
  @ViewChild('f', { static: true }) form!: NgForm;
  @Input() active: Product | null = null;
  @Output() save = new EventEmitter()
  @Output() reset = new EventEmitter()

  ngOnChanges() {
    if (!this.active?.id) {
      this.form.reset();
    }
  }

  saveHandler(f: NgForm) {
    this.save.emit(f.value)
   // f.reset()
  }

  resetHandler(f: NgForm) {
    this.reset.emit()
    f.reset()
  }
}
