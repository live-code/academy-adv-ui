import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { Product } from '../../../../model/product';

@Component({
  selector: 'ac-catalog-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    
    <hr>
    {{filterGender}}
    <select [(ngModel)]="filterGender">
      <option value="all">All genders</option>
      <option value="M">male</option>
      <option value="F">female</option>
    </select>
    <select [(ngModel)]="sortType">
      <option value="ASC">ASC</option>
      <option value="DESC">DESC</option>
    </select>
    
    <input type="text" [(ngModel)]="textToSearch">
    <li
      *ngFor="let product of (products 
                  | filterByGender: filterGender
                  | filterByText: textToSearch
                  | sort: sortType
                )"
      (click)="selectProduct.emit(product)"
      [style.background-color]="product.id === activeProduct?.id ? 'orange' : null"
    >
      {{product.name}} - {{product.gender}}
      <button (click)="deleteProduct.emit(product)">delete</button>
    </li>
  `,
})
export class CatalogListComponent {
  @Input() products: Product[] = [];
  @Input() activeProduct: Product | null = null;
  @Output() selectProduct = new EventEmitter<Product>();
  @Output() deleteProduct = new EventEmitter<Product>();
  filterGender = 'all'
  sortType: 'ASC' | 'DESC' = 'ASC'
  textToSearch = '';
}
