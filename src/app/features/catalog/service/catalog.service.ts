import { Injectable } from '@angular/core';
import { Product } from '../../../../model/product';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';


@Injectable()
export class CatalogService {
  products: Product[] = [];
  activeProduct: Product = { } as Product;

  constructor(private http: HttpClient) {}

  getProducts() {
    this.http.get<Product[]>(`${environment.API}/products`)
      .subscribe(res => {
        this.products = res;
      })
  }

  deleteProduct(product: Product) {
    this.http.delete(`${environment.API}/products/${product.id}`)
      .subscribe(
        () =>  this.products = this.products.filter(p => p.id !== product.id),
      )
  }


  save(formData: any) {
    if (this.activeProduct?.id) {
      this.edit(formData)
    }   else {
      this.add(formData)
    }
  }

  edit(formData: any) {
    this.http.patch<Product>(`${environment.API}/products/${this.activeProduct?.id}`, formData)
      .subscribe(res => {
        this.products = this.products.map(p => {
          return p.id === this.activeProduct?.id ? res : p
        })
      })
  }

  add(formData: any) {
    this.http.post<Product>(`${environment.API}/products/`, formData)
      .subscribe(res => {
        this.products = [...this.products, res]
        this.activeProduct  = {} as Product
      })
  }

  selectProduct(product: Product) {
    this.activeProduct = product;
  }
  reset() {
    this.activeProduct = {} as Product
  }
}
