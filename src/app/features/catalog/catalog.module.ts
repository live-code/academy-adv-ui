import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CatalogRoutingModule } from './catalog-routing.module';
import { CatalogComponent } from './catalog.component';
import { FormsModule } from '@angular/forms';
import { CatalogListComponent } from './components/catalog-list.component';
import { CatalogFormComponent } from './components/catalog-form.component';
import { CatalogService } from './service/catalog.service';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [
    CatalogComponent,
    CatalogListComponent,
    CatalogFormComponent
  ],
  imports: [
    CommonModule,
    CatalogRoutingModule,
    FormsModule,
    SharedModule
  ],
  providers: [
    CatalogService
  ]
})
export class CatalogModule { }
