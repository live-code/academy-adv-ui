import { Component, OnInit } from '@angular/core';
import { CatalogService } from './service/catalog.service';
import { HttpClient } from '@angular/common/http';
import { User } from '../../shared/pipes/user.pipe';
import { map } from 'rxjs';


@Component({
  selector: 'ac-catalog',
  template: `


    <ac-catalog-form
      [active]="catalogService.activeProduct"
      (save)="catalogService.save($event)"
      (reset)="catalogService.reset()"
    ></ac-catalog-form>

    <ac-catalog-list
      [products]="catalogService.products"
      [activeProduct]="catalogService.activeProduct"
      (selectProduct)="catalogService.selectProduct($event)"
      (deleteProduct)="catalogService.deleteProduct($event)"
    ></ac-catalog-list>

    <img [src]="'trieste' | mapquest" alt="" width="100%">

    {{value | formatTo: 'mb'}}
    {{value | formatTo: 'bytes': 'b' }}
    <button (click)="value = value + 1">+</button>

    <h1>{{(value | user | async )}}</h1>
    h
    <div>{{(something | obj )}}</div>
  `,
})
export class CatalogComponent implements OnInit {
  value = 1;
  something = { id: 123 }

  constructor(public catalogService: CatalogService) {

      setTimeout(() => {
        this.something = { id: 456 }
      }, 2000)
  }

  ngOnInit(): void {
    this.catalogService.getProducts();
  }

  formatToMb(value: number) {
    return value * 1000;
  }
}
