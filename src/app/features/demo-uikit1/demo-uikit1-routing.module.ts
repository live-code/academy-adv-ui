import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DemoUikit1Component } from './demo-uikit1.component';

const routes: Routes = [{ path: '', component: DemoUikit1Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoUikit1RoutingModule { }
