import { Component, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { PanelComponent } from '../../shared/components/panel.component';

@Component({
  selector: 'ac-demo-uikit1',
  template: `
    <ac-accordion #accordion>
      <ac-panel title="pluto">
        <input type="text">
        <input type="text">
        <input type="text">
      </ac-panel>
      
      <ac-panel title="pippo">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi consequuntur eaque est excepturi expedita hic ipsum neque nihil nobis, odio optio sequi, tempora velit? Alias amet consectetur magnam quam voluptatum.
      </ac-panel>
      
      <ac-panel title="pluto">
        <input type="text">
        <input type="text">
        <input type="text">
      </ac-panel>
    </ac-accordion>
    
    <button (click)="accordion.openAll()">Open All</button>
    <button (click)="accordion.closeAll()">Close All</button>
  `,
})
export class DemoUikit1Component {


}
