import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DemoUikit2Component } from './demo-uikit2.component';

const routes: Routes = [{ path: '', component: DemoUikit2Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoUikit2RoutingModule { }
