import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoUikit2RoutingModule } from './demo-uikit2-routing.module';
import { DemoUikit2Component } from './demo-uikit2.component';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    DemoUikit2Component
  ],
  imports: [
    CommonModule,
    SharedModule,
    DemoUikit2RoutingModule,
    FormsModule
  ]
})
export class DemoUikit2Module { }
