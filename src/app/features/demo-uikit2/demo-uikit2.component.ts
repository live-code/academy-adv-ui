import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ac-demo-uikit2',
  template: `
    
    <h1>URL</h1>
    <button acUrl="http://www.google.com">OPEN</button>
    <div acUrl="http://www.google.com">OPEN</div>
    <span acUrl="http://www.facebook.com">OPEN</span>
    
    <h1>Stop Propagation</h1>
    <div (click)="parent()" class="bg-warning p-4">
      <div acStopPropagation (click)="child()" class="bg-info p-4">
        contenuto
      </div>
    </div>    
    
  
    
    <hr>
    <h1>Grid</h1>
    <ac-grid mq="md">
      <ac-col [size]="12">1</ac-col>
      <ac-col [size]="3">2</ac-col>
      <ac-col [size]="3">3</ac-col>
    </ac-grid>
    
    <div acTheme>themed</div>
    
     <div
       (click)="padding = padding + 1"
       [acPad]="padding">padding {{padding}}</div>
    
     <div acBg="red" [p]="5" >bg example</div>
     <div acBg="green" >bg example</div>
    
  `,
})
export class DemoUikit2Component  {
  padding: number = 5;

  parent() {
    console.log('parent')
  }
  child() {
    console.log('child')
  }
}
