import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'ac-demo-lifecycle',
  template: `

    <ac-panel title="bla lba" >
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus harum minima perferendis quis! Architecto cumque delectus dolorem fugit illum iure labore, laudantium molestias, perspiciatis quos ratione recusandae sed suscipit veritatis!
    </ac-panel>
    <ac-highcharts
      [temperatures]="chartConfig"
    ></ac-highcharts>

    <h1>Weather</h1>
    <ac-weather [city]="myCity" [unit]="myUnit"></ac-weather>
    <hr>
    <button (click)="myCity = 'palermo'">Palermo</button>
    <button (click)="myCity = 'milano'">Milano</button>
    <button (click)="myUnit = 'imperial'">imperial</button>
    <button (click)="myUnit = 'metric'">metric</button>
    
    
    <ac-leaflet 
      [coords]="coords"
      [zoom]="myZoom"
      (markerClick)="doSomething($event)"
    ></ac-leaflet>
    
    <button (click)="coords = [43, 13]">Roma</button>
    <button (click)="coords = [41, 11]">Non so!</button>
    <button (click)="myZoom = myZoom + 1">+</button>
    <button (click)="myZoom = myZoom - 1">-</button>
   
    <hr>
    <input type="text" ngModel>
    {{render()}}
  `,

})
export class DemoLifecycleComponent  {
  open= true;
  coords: [number, number] = [51, 50]
  myCity: string | null = 'Roma'
  myUnit: string = 'metric'
  myZoom: number = 5;

  chartConfig = {
    name: 'Summer',
    data: [10, 20, 30, 40, 30, 20]
  }

  constructor() {
    setTimeout(() => {
      this.chartConfig = {
        name: 'Winter',
          data: [0, 5, 10, 20, 30, 20]
      }
    }, 2000)
  }

  doSomething(params: { lat: number, lng: number}) {
    console.log(params.lat)
  }

  render() {
    console.log('parent render')
  }
}
