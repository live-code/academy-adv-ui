import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoLifecycleRoutingModule } from './demo-lifecycle-routing.module';
import { DemoLifecycleComponent } from './demo-lifecycle.component';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    DemoLifecycleComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    DemoLifecycleRoutingModule,
    FormsModule
  ]
})
export class DemoLifecycleModule { }
