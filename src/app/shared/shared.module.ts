import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WeatherComponent } from './components/weather.component';
import { LeafletComponent } from './components/leaflet.component';
import { HighchartsComponent } from './components/highcharts.component';
import { PanelComponent } from './components/panel.component';
import { AccordionComponent } from './components/accordion.component';
import { GridComponent } from './components/grid.component';
import { ColComponent } from './components/col.component';
import { ListComponent } from './components/list.component';
import { BgDirective } from './directives/bg.directive';
import { PDirective } from './directives/p.directive';
import { ThemeDirective } from './directives/theme.directive';
import { UrlDirective } from './directives/url.directive';
import { StopPropagationDirective } from './directives/stop-propagation.directive';
import { MyRouterLinkActiveDirective } from './directives/my-router-link-active.directive';
import { IfLoggedDirective } from './directives/if-logged.directive';
import { FormatToPipe } from './pipes/format-to.pipe';
import { MapquestPipe } from './pipes/mapquest.pipe';
import { FilterByGenderPipe } from './pipes/filter-by-gender.pipe';
import { FilterByTextPipe } from './pipes/filter-by-text.pipe';
import { SortPipe } from './pipes/sort.pipe';
import { UserPipe } from './pipes/user.pipe';
import { ObjPipe } from './pipes/obj.pipe';



@NgModule({
  declarations: [
    WeatherComponent,
    LeafletComponent,
    HighchartsComponent,
    PanelComponent,
    AccordionComponent,
    GridComponent,
    ColComponent,
    ListComponent,
    BgDirective,
    PDirective,
    ThemeDirective,
    UrlDirective,
    StopPropagationDirective,
    MyRouterLinkActiveDirective,
    IfLoggedDirective,
    FormatToPipe,
    MapquestPipe,
    FilterByGenderPipe,
    FilterByTextPipe,
    SortPipe,
    UserPipe,
    ObjPipe
  ],
  exports: [
    WeatherComponent,
    LeafletComponent,
    HighchartsComponent,
    PanelComponent,
    AccordionComponent,
    GridComponent,
    ColComponent,
    ListComponent,
    BgDirective,
    PDirective,
    ThemeDirective,
    UrlDirective,
    StopPropagationDirective,
    MyRouterLinkActiveDirective,
    IfLoggedDirective,
    FormatToPipe,
    MapquestPipe,
    FilterByGenderPipe,
    FilterByTextPipe,
    SortPipe,
    UserPipe,
    ObjPipe
  ],
  imports: [
    CommonModule
  ]
})
export class SharedModule { }
