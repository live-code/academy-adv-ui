import { Pipe, PipeTransform } from '@angular/core';
import { Product } from '../../../model/product';

@Pipe({
  name: 'sort'
})
export class SortPipe implements PipeTransform {

  transform(products: Product[], order: 'ASC' | 'DESC' = 'ASC'): Product[]{
    const result = products.sort((a, b) => a.name.localeCompare(b.name));
   return order === 'ASC' ? result : result.reverse();
  }

}
