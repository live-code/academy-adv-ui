import { Pipe, PipeTransform } from '@angular/core';
import { Product } from '../../../model/product';

@Pipe({
  name: 'filterByGender'
})
export class FilterByGenderPipe implements PipeTransform {

  transform(items: Product[], gender: string ): Product[] {
    if (gender === 'all') {
      return items;
    }
    return items.filter(item => item.gender === gender);
  }

}
