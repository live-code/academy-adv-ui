import { Pipe, PipeTransform } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, Observable } from 'rxjs';

export interface User {
  id: number;
  name: string;
}

@Pipe({
  name: 'user'
})
export class UserPipe implements PipeTransform {

  constructor(private http: HttpClient) { }

  transform(userId: number): Observable<string> {
    return this.http.get<User>('https://jsonplaceholder.typicode.com/users/' + userId)
      .pipe(
        map(res => res.name)
      )
  }

}
