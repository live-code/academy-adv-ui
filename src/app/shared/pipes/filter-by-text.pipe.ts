import { Pipe, PipeTransform } from '@angular/core';
import { Product } from '../../../model/product';

@Pipe({
  name: 'filterByText'
})
export class FilterByTextPipe implements PipeTransform {

  transform(products: Product[], textToFind: string): Product[] {
    return products.filter(p => {
      return p.name.toLowerCase().includes(textToFind.toLowerCase())
    });
  }


}
