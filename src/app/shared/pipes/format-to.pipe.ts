import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatTo',
})
export class FormatToPipe implements PipeTransform {

  transform(value: number, formatTo: 'mb' | 'bytes', prefix: string = ''): string {
    switch (formatTo) {
      case 'mb': return value * 1000 + prefix;
      case 'bytes': return value * 10000000 + prefix;
    }
  }

}
