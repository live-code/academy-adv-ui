import {
  AfterContentInit,
  ChangeDetectorRef,
  Component,
  ContentChildren,
  OnInit,
  QueryList,
  ViewChildren
} from '@angular/core';
import { PanelComponent } from './panel.component';

@Component({
  selector: 'ac-accordion',
  template: `
    <div style="border:4px solid black; padding: 20px">
      <ng-content></ng-content>
    </div>
  `,
})
export class AccordionComponent implements AfterContentInit {
  @ContentChildren(PanelComponent) panels!: QueryList<PanelComponent>;

  ngAfterContentInit() {
    this.panels.toArray().forEach(panel => {
      panel.open = false;
      panel.headerClick.subscribe(() => {
        this.closeAll()
        panel.open = true;
      })
    })
    this.panels.toArray()[0].open = true;
  }

  closeAll() {
    this.panels.toArray().forEach(panel => {
      panel.open = false
    })
  }

  openAll() {
    this.panels.toArray().forEach(panel => {
      panel.open = true
    })
  }

}
