import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'ac-grid',
  template: `
    <div class="row">
      <ng-content></ng-content>
    </div>
  `,
})
export class GridComponent {
  @Input() mq: 'sm'  | 'md' | 'lg' = 'sm'
}
