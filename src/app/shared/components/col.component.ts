import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { GridComponent } from './grid.component';

@Component({
  selector: 'ac-col',
  template: `
    <ng-content></ng-content>
  `,
  styles: []
})
export class ColComponent  {
  @Input() size: 1 | 2 | 3 | 4 | 5 | 6 |7 | 8 | 9 | 10 | 11 | 12 = 12;

  @HostBinding() get class() {
    return 'col-' + this.parent.mq + '-' + this.size;
  }

  constructor(private parent: GridComponent ) {}

}

