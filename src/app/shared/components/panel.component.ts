import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ac-panel',
  // changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="card mb-2">
      <div class="card-header" (click)="headerClick.emit()">{{title}}</div>
      <div class="card-body" *ngIf="open">
        <ng-content></ng-content>
      </div>
    </div>
  `,
})
export class PanelComponent {
  @Input() title: string = '';
  @Input() open = true;
  @Output() headerClick = new EventEmitter()
}
