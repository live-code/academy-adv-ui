import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import * as L from 'leaflet';
import { LatLngTuple } from 'leaflet';

@Component({
  selector: 'ac-leaflet',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div #mapRef class="map" ></div>
  `,
  styles: [`
    .map { height: 200px; width: 100%; background-color: red}
  `]
})
export class LeafletComponent implements OnChanges {
  @ViewChild('mapRef',  { static: true }) mapRef!: ElementRef<HTMLDivElement>
  @Input() coords: number[] | null = null;
  @Input() zoom: number = 5
  @Output() markerClick = new EventEmitter<any>()

  map!: L.Map;
  marker!: L.Marker;

  ngOnChanges(changes:SimpleChanges) {
    if (changes['coords'] && changes['coords'].firstChange) {
      this.map = L.map(this.mapRef.nativeElement).setView(this.coords as LatLngTuple, this.zoom);
      this.marker = L.marker(this.coords as LatLngTuple)
        .bindPopup('A pretty CSS3 popup.<br> Easily customizable.')
        .openPopup()
        .addTo(this.map);

      this.marker.addEventListener('click', (e: any) => {
        this.markerClick.emit({ lat: e.latlng.lat, lng: e.latlng.lng, })
      })

      L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
      }).addTo(this.map);
    }

    if (changes['coords']) {
      this.map.flyTo(this.coords as LatLngTuple)

      this.marker.setLatLng(this.coords as LatLngTuple);
    }
    if (changes['zoom']) {
      this.map.setZoom(this.zoom)
    }
  }

  render() {
    console.log('leaflet render')
  }
}
