import { Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import * as Highcharts  from 'highcharts';
import { Chart } from 'highcharts';
@Component({
  selector: 'ac-highcharts',
  template: `
    <div #host style="width:100%; height:400px;"></div>
  `,
})
export class HighchartsComponent implements OnChanges {
  @ViewChild('host', { static: true }) host!: ElementRef<HTMLDivElement>;
  @Input() temperatures: { name: string, data: number[] } | null = null;
  chart!: Chart;

  ngOnChanges(changes: SimpleChanges) {
    if (changes['temperatures'].isFirstChange()) {
      // init chart
      this.chart = Highcharts.chart(
        getConfig( this.host.nativeElement, this.temperatures as any)
      );
    } else {
      // update chart
      this.chart.update(getConfig(this.host.nativeElement, this.temperatures as any))
    }
  }

}


export function getConfig(el: HTMLElement, data: any):  Highcharts.Options  {
  return  {
    chart: {
      renderTo: el,
      type: 'column'
    },
    title: {
      text: 'Fruit Consumption'
    },
    xAxis: {
      categories: ['L', 'M', 'M', 'G', 'V', 'S']
    },
    yAxis: {
      title: {
        text: 'Temperatures'
      }
    },
    series: [data]
  }
}
