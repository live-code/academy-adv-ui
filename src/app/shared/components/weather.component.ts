import { ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'ac-weather',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <h2 *ngIf="city; else nocity">City is {{city}}</h2>
    <ng-template #nocity>NO CITY</ng-template>
    
    <div *ngIf="meteo">
      {{meteo.main?.temp}} °
      <img [src]="'http://openweathermap.org/img/w/' + meteo.weather[0]?.icon + '.png'" alt="">
    </div>

  `,
})
export class WeatherComponent implements OnChanges {
  @Input() city: string | null = null;
  @Input() unit: string = 'metric';

  meteo: Meteo | null = null;

  constructor(private http: HttpClient) {}

  ngOnChanges(changes: SimpleChanges) {
    this.searchMeteo();
  }

  searchMeteo(): void {
    if (this.city) {
      this.http.get<Meteo>(`https://api.openweathermap.org/data/2.5/weather?q=${this.city}&units=${this.unit}&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
        .subscribe(res => {
          this.meteo = res;
        })
    }
  }


}


// TYPE
// model/meteo.ts

export interface Coord {
  lon: number;
  lat: number;
}

export interface Weather {
  id: number;
  main: string;
  description: string;
  icon: string;
}

export interface Main {
  temp: number;
  feels_like: number;
  temp_min: number;
  temp_max: number;
  pressure: number;
  humidity: number;
}

export interface Wind {
  speed: number;
  deg: number;
  gust: number;
}

export interface Clouds {
  all: number;
}

export interface Sys {
  type: number;
  id: number;
  country: string;
  sunrise: number;
  sunset: number;
}

export interface Meteo {
  coord: Coord;
  weather: Weather[];
  base: string;
  main: Main;
  visibility: number;
  wind: Wind;
  clouds: Clouds;
  dt: number;
  sys: Sys;
  timezone: number;
  id: number;
  name: string;
  cod: number;
}

