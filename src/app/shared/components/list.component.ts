import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'ac-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <li *ngFor="let item of data">{{item}}</li>
  `,
})
export class ListComponent {
  @Input() data: string[] = []

}
