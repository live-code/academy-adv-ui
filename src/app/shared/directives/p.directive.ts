import { Directive, ElementRef, HostBinding, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[acPad]'
})
export class PDirective {
/*  @Input() set acPad(val: number) {
    this.renderer.setStyle(this.el.nativeElement, 'margin', val * 10 + 'px')
  }*/
  @Input() acPad: number = 0

  constructor(
    private el: ElementRef<HTMLElement>,
    private renderer: Renderer2
  ) {
  }

  ngOnChanges() {
    this.renderer.setStyle(this.el.nativeElement, 'margin', this.acPad * 10 + 'px')
  }
}
