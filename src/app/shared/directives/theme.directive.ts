import { Directive, HostBinding } from '@angular/core';
import { ThemeService } from '../../core/services/theme.service';

@Directive({
  selector: '[acTheme]'
})
export class ThemeDirective {
  @HostBinding() get class()  {
    return this.themeService.value === 'dark' ?
      'bg-dark text-white' :
      'bg-warning'
  }

  constructor(private themeService: ThemeService) {
  }

}
