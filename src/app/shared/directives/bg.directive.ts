import { Directive, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[acBg]'
})
export class BgDirective {
  @HostBinding('style.background') get background() {
    return this.bg || 'yellow';
  }
  @HostBinding()  get class () {
    return `p-${this.p}`
  }

  @Input() bg: string | undefined;
  @Input() p: 0 | 1 | 2 | 3 | 4 | 5  = 0
}
