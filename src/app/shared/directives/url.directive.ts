import { Directive, HostBinding, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[acUrl]'
})
export class UrlDirective {
  @Input() acUrl!: string;
  @HostBinding('style.cursor') cursor = 'pointer'

  @HostListener('click', ['$event'])
  openUrl(event: MouseEvent) {
    window.open(this.acUrl);
  }


}
