import { Directive, ElementRef, HostBinding, Input, Renderer2, TemplateRef, ViewContainerRef } from '@angular/core';
import { AuthService } from '../../core/services/auth.service';

@Directive({
  selector: '[acIfLogged]'
})
export class IfLoggedDirective {
  @Input('acIfLogged') requiredRole!: string;

  constructor(
    private auth: AuthService,
    private tpl: TemplateRef<any>,
    private renderer: Renderer2,
    private view: ViewContainerRef,
  ) {
    this.auth.data
      .subscribe(data => {
        if (data && data.role === this.requiredRole) {
          this.view.createEmbeddedView(this.tpl)
        } else {
          this.view.clear();
        }
      })
  }

}
