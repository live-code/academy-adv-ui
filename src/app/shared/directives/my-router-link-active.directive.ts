import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';
import { NavigationEnd, Router, RouterLink } from '@angular/router';

@Directive({
  selector: '[acMyRouterLinkActive]'
})
export class MyRouterLinkActiveDirective {
  @Input() acMyRouterLinkActive!: string;

  constructor(
    private router: Router,
    private el: ElementRef<HTMLElement>,
    private renderer2: Renderer2,
    private routerLink: RouterLink
  ) {
    const routerLinkUrl = routerLink['commands'][0]
    this.router.events
      .subscribe(ev => {
        if (ev instanceof NavigationEnd && routerLinkUrl) {
          if (ev.url.includes(routerLinkUrl)) {
            this.renderer2.addClass(
              this.el.nativeElement,
              this.acMyRouterLinkActive
            )
          } else {
            this.renderer2.removeClass(
              this.el.nativeElement,
              this.acMyRouterLinkActive
            )
          }
        }
      })
  }

}
