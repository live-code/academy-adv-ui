import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

export interface Auth {
  token: string;
  role: string;
  displayName: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  data = new BehaviorSubject<Auth | null>(null)

  login() {
    // ... http
    this.data.next({ token: '123', role: 'admin', displayName: 'Pippo Baudo' })
  }

  logout() {
    this.data.next(null)
  }

  get ifLogged(): boolean {
    return !!this.data;
  }
}
