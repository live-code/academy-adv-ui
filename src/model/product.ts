export interface Product {
  id: number;
  name: string;
  gender: string;
}
